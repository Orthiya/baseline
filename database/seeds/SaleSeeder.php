<?php

use Illuminate\Database\Seeder;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            [
                'id' => 1,
                'amount' => 1.85,
                'cups' => 1,
            ],
            [
                'id' => 2,
                'amount' => 2.85,
                'cups' => 1,
            ],
            [
                'id' => 3,
                'amount' => 3.85,
                'cups' => 1,
            ],
            [
                'id' => 4,
                'amount' => 1.75,
                'cups' => 1,
            ],
            [
                'id' => 5,
                'amount' => 6.5,
                'cups' => 2,
            ],

        ];
        \Illuminate\Support\Facades\DB::table('sales')->insert($data);
    }
}
