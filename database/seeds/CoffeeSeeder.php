<?php

use Illuminate\Database\Seeder;

class CoffeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            [
                'id' => 1,
                'price' => 1.85,
            ],
            [
                'id' => 2,
                'price' => 2.85,
            ],
            [
                'id' => 3,
                'price' => 3.85,
            ],
            [
                'id' => 4,
                'price' => 1.75,
            ],
            [
                'id' => 5,
                'price' => 3.25,
            ],

        ];

        $coffee_product = [
            [
                'coffee_id' => 1,
                'product_id' => 1
            ],
            [
                'coffee_id' => 1,
                'product_id' => 3
            ],
            [
                'coffee_id' => 2,
                'product_id' => 2
            ],
            [
                'coffee_id' => 2,
                'product_id' => 3
            ],
            [
                'coffee_id' => 3,
                'product_id' => 2
            ],
            [
                'coffee_id' => 3,
                'product_id' => 3
            ],
            [
                'coffee_id' => 3,
                'product_id' => 6
            ],
            [
                'coffee_id' => 4,
                'product_id' => 1
            ],
            [
                'coffee_id' => 4,
                'product_id' => 4
            ],
            [
                'coffee_id' => 5,
                'product_id' => 1
            ],
            [
                'coffee_id' => 5,
                'product_id' => 6
            ],
            [
                'coffee_id' => 5,
                'product_id' => 5
            ],
        ];

        \Illuminate\Support\Facades\DB::table('coffees')->insert($data);
        \Illuminate\Support\Facades\DB::table('coffee_product')->insert($coffee_product);

    }
}
