<?php

use Illuminate\Database\Seeder;

class CoffeeProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        -	Base ingredient: Small Cup Of Coffee priced at $1.00
        -	Base ingredient: Large Cup Coffee priced at $2.00
        -	Add-on ingredient: 1 pump of chocolate priced at $0.85
        -	Add-on ingredient: 1 pump of vanilla priced at $0.75
        -	Add-on ingredient: 1 oz whole milk priced at $1.25
        -	Add-on ingredient: 1 oz skim milk priced at $1.00
         */

        $data = [
            [
                'id' => 1,
                'name' => 'Small Cup',
                'price' => 1.00,
                'type' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Large Cup',
                'price' => 2.00,
                'type' => 1,
            ],
            [
                'id' => 3,
                'name' => 'chocolate',
                'price' => 0.85,
                'type' => 1,
            ],
            [
                'id' => 4,
                'name' => 'vanilla',
                'price' => 0.75,
                'type' => 1,
            ],
            [
                'id' => 5,
                'name' => 'whole milk',
                'price' => 1.25,
                'type' => 1,
            ],
            [
                'id' => 6,
                'name' => 'skim milk',
                'price' => 1.00,
                'type' => 1,
            ],
        ];
        \Illuminate\Support\Facades\DB::table('products')->insert($data);
    }
}
