<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/coffee', 'CoffeeController@store');
Route::get('/coffee/copy/{id}', 'CoffeeController@copy');
Route::get('/coffeeAddOn/{id}', 'CoffeeController@getAddOns');
Route::get('/coffeePrice/{id}', 'CoffeeController@getPrice');


Route::get('/sales', 'SalesController@create');