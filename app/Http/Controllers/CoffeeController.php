<?php

namespace App\Http\Controllers;

use App\Coffee;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CoffeeController extends Controller
{

    /*
     *  Stores Coffee
     */
    public function store()
    {
        $product = Input::get('product');;
        if($product == null)
        {
            return response()->json([
                'error' => 'We need some product to make you coffee',
                'status' => 404
            ], 404);
        }
        $array = explode(',', $product);
        foreach ($array as $check)
        {
            if(Product::find($check))
            {
                continue;
            }
            else{
                return response()->json([
                    'error' => 'A product id do not exist.',
                    'Incorrect id' => $check,
                    'status' => 500
                ], 500);
            }
        }
        if(!in_array(1,$array) and  !in_array(2,$array))
        {
            return response()->json([
                'error' => 'Hold On! We need a cup! ;)',
                'msg' => 'Please add either 1 or 2, You can only add one!',
                'status' => 402
            ], 402);
        }

        if(in_array(1,$array) and  in_array(2,$array))
        {
            return response()->json([
                'error' => 'Only need a single cup',
                'msg' => 'Please select either 1 or 2 ',
                'status' => 402
            ], 402);
        }


        $coffee = new Coffee();
        $coffee->price = $this->totalPrice($array);
        $coffee->save();
        $coffee->product()->sync($array);
        return response()->json([
            'msg' => 'Coffee record made successfully',
            'price' => $coffee->price,
            'status' => 200
        ], 200);
    }

    /*
     *  Copy Store
     */

    public function copy($id)
    {
        $remove = Input::get('remove');
        $array = explode(',', $remove);
        foreach ($array as $check)
        {
            if(Product::find($check))
            {
                continue;
            }
            else{
                return response()->json([
                    'error' => 'A product id do not exist.',
                    'Incorrect id' => $check,
                    'status' => 500
                ], 500);
            }
        }

        if(Coffee::find($id))
        {
            $coffee1 = Coffee::find($id);
            foreach ($coffee1->product as $pro)
            {
                $coffee1_products[] =  $pro->id;
            }

            $array = $coffee1_products;
            foreach (array_keys($array, $remove) as $key) {
                unset($array[$key]);
            }


            $coffee2 = new Coffee();
            $coffee2->price = $this->totalPrice($array);
            $coffee2->save();
            $coffee2->product()->sync($array);
            return response()->json([
                'msg' => 'Duplicated Coffee record made successfully',
                'price' => $coffee2->price,
                'status' => 200
            ], 200);
        }
    }

    /*
     *  Get Adds On in the Coffee
     */

    public function getAddOns($id)
    {
        if(Coffee::find($id)){
            $coffee = Coffee::find($id);
            foreach ($coffee->product as $pro)
            {
                $name[] =  $pro->name;
            }
            return $name;
        }
    }

    /*
     *  total Price of the coffee
     */

    private function totalPrice($products)
    {
        $total = 0;
        foreach ($products as $product)
        {
            $product = Product::find($product);
            $total = $total + $product->price;
        }
        return $total;
    }

    /*
     *  get prices
     */

    public function getPrice($id)
    {
        $coffee = Coffee::find($id);
        return $coffee->price;
    }


}
