<?php

namespace App\Http\Controllers;

use App\Coffee;
use App\Jobs\SendThankYouEmail;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SalesController extends Controller
{
    //
    
    protected $coffee;
    public function __construct(CoffeeController $coffee)
    {
        $this->coffee = $coffee;
    }

    public function recordSale($ids)
    {
        $price = null;
        foreach ($ids as $id)
        {
            $price += $this->coffee->getPrice($id);
        }
        return $price;
    }


    
    public function create()
    {
        $coffee = Input::get('coffee');;
        if($coffee == null)
        {
            return response()->json([
                'error' => 'We need some coffee to show you sale.',
                'status' => 404
            ], 404);
        }
        $array = explode(',', $coffee);
        foreach ($array as $check)
        {
            if(Coffee::find($check))
            {
                continue;
            }
            else{
                return response()->json([
                    'error' => 'A coffee id do not exist.',
                    'Incorrect id' => $check,
                    'status' => 500
                ], 500);
            }
        }

        $sale = new Sale();
        $sale->amount = $this->recordSale($array);
        $sale->cups = sizeof($array);
        if($sale->save())
        {
            $this->dispatch(new SendThankYouEmail());
            $time = Carbon::parse($sale->created_at);

            return response()->json([
                'msg' => 'Sale record made successfully',
                'price' => number_format((float)$sale->amount, 2, '.', ''),
                'time' => 'Recorded Time from Database '.$time->format('h:i:s'),
                'status' => 200
            ], 200);
        }
        else
        {
            return response()->json([
                'error' => 'Sale record can not be created.',
                'status' => 402
            ], 402);
        }
    }
}
