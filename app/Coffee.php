<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coffee extends Model
{

    public function product()
    {
        return $this->belongsToMany('App\Product', 'coffee_product');
    }

}
