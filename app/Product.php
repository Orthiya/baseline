<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public $timestamps = false;

    public function coffee()
    {
        return $this->belongsToMany('App\Coffee', 'coffee_product');
    }
}
